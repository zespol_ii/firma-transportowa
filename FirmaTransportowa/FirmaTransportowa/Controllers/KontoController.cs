﻿using FirmaTransportowa.Filters;
using FirmaTransportowa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace FirmaTransportowa.Controllers
{
    public class KontoController : Controller
    {
        //
        // GET: /Konto/

        #region Logowanie
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(OSOBA u)
        {
            //System.Diagnostics.Debug.WriteLine(ModelState.Where(x => x.Value.Errors.Any())
              // .Select(x => new { x.Key, x.Value.Errors }));

            System.Diagnostics.Debug.WriteLine(String.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                                                           .Select(v => v.ErrorMessage + " " + v.Exception)));

            // this action is for handle post (login)
            if (ModelState.IsValid) // this is check validity
            {
                using ( FirmaTransportowa.Models.FirmaJPEntities1 dc = new FirmaJPEntities1())
                {
                    var v = dc.OSOBY.Where(a => a.OSO_LOGIN.Equals(u.OSO_LOGIN) && a.OSO_HASLO.Equals(u.OSO_HASLO)).FirstOrDefault();
                    if (v != null)
                    {
                        Session["LogedUserID"] = v.OSO_ID.ToString();
                        Session["LogedUserFullname"] = v.OSO_IMIE.ToString() + " " + v.OSO_NAZWISKO.ToString();
                        FormsAuthentication.SetAuthCookie(v.OSO_ID.ToString(), false);

                        if ((dc.ADMINISTRACJA.Any(a => a.OSO_ID == v.OSO_ID)))
                            return RedirectToAction("AfterLogin","Administracja");
                        return RedirectToAction("AfterLogin");
                    }
                    ModelState.AddModelError("", "Podana nazwa lub hasło sa nieprawidłowe.");
                }
            } 
            return View(u);
        }

        public ActionResult AfterLogin()
        {
            if (Request.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Blad","Home");
            }
        }
        #endregion Logowanie

        #region Rejestracja
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(OSOBA U)
        {
           
            if (ModelState.IsValid)
            {
                using (FirmaTransportowa.Models.FirmaJPEntities1 dc = new FirmaJPEntities1())
                {
                    //you should check duplicate registration here 
                    //OMG to zakrawa na czyste szleństwo :(
                    KLIENT k = new KLIENT();
                    k.OSOBA = new OSOBA();
                    k.OSOBA = U;
                    //k.OSOBA.DANE_TELEADRESOWE = new DANE_TELEADRESOWE();
                    //ID klienta
                    if (dc.KLIENCI.Max(u => (int?)u.KLI_ID) != null)
                        k.KLI_ID = dc.KLIENCI.Max(u => u.KLI_ID) + 1;
                    else
                        k.KLI_ID = 1;

                    //ID osoby
                    if (dc.OSOBY.Max(u => (int?)u.OSO_ID) != null)
                    {
                        
                        k.OSO_ID = dc.OSOBY.Max(u => u.OSO_ID) + 1;
                        k.OSOBA.OSO_ID = dc.OSOBY.Max(u => u.OSO_ID) + 1;
                    }
                    else
                    {                      
                        k.OSOBA.OSO_ID = 1;
                        k.OSO_ID = 1;
                    }

                    if (dc.OSOBY.Max(u => (int?)u.DAN_ID) != null)
                    {

                        k.OSOBA.DAN_ID = dc.OSOBY.Max(u => u.DAN_ID) + 1;
                        k.OSOBA.DANE_TELEADRESOWE.DAN_ID = (int)k.OSOBA.DAN_ID;
                    }
                    else
                    {
                        k.OSOBA.DAN_ID = 1;
                        k.OSOBA.DANE_TELEADRESOWE.DAN_ID = 1;
                    }

                    //k.OSOBA = U;
                    //k.OSOBA.OSO_ID = i;
                    
                    /*//ID danych
                    if (dc.DANE_TELEADRESOWE.Max(u => (int?)u.DAN_ID) != null)
                    {                        
                        k.OSOBA.DAN_ID = dc.DANE_TELEADRESOWE.Max(u => (int)u.DAN_ID) + 1;
                        k.OSOBA.DANE_TELEADRESOWE.DAN_ID = dc.DANE_TELEADRESOWE.Max(u => (int)u.DAN_ID) + 1;
                    }
                    else
                    {
                        k.OSOBA.DANE_TELEADRESOWE.DAN_ID = 1;
                        k.OSOBA.DAN_ID = 1;
                    }
                    */
                 
                    //osoba
                    //k.OSOBA.OSO_HASLO = U.OSO_HASLO;
                    //k.OSOBA.OSO_IMIE = U.OSO_IMIE;
                    //k.OSOBA.OSO_LOGIN = U.OSO_LOGIN;
                    //k.OSOBA.OSO_NAZWISKO = U.OSO_NAZWISKO;
                    //k.OSOBA.OSO_URODZONY = U.OSO_URODZONY;

                    //dane
                    //k.OSOBA.DANE_TELEADRESOWE.DAN_EMAIL = U.DANE_TELEADRESOWE.DAN_EMAIL;
                    //k.OSOBA.DANE_TELEADRESOWE.DAN_KOD_POCZTOWY = U.DANE_TELEADRESOWE.DAN_KOD_POCZTOWY;
                    //k.OSOBA.DANE_TELEADRESOWE.DAN_MIASTO = U.DANE_TELEADRESOWE.DAN_MIASTO;
                    //k.OSOBA.DANE_TELEADRESOWE.DAN_TELEFON = U.DANE_TELEADRESOWE.DAN_TELEFON;
                    //k.OSOBA.DANE_TELEADRESOWE.DAN_ULICA_NR_LOKALU = U.DANE_TELEADRESOWE.DAN_ULICA_NR_LOKALU;

                    //U.DANE_TELEADRESOWE.DAN_ID = D.DAN_ID;
                    //D.DAN_EMAIL = U.DANE_TELEADRESOWE.DAN_EMAIL;
                    //D.DAN_KOD_POCZTOWY = U.DANE_TELEADRESOWE.DAN_KOD_POCZTOWY;
                    //D.DAN_MIASTO = U.DANE_TELEADRESOWE.DAN_MIASTO;
                    //D.DAN_TELEFON = U.DANE_TELEADRESOWE.DAN_TELEFON;
                    //D.DAN_ULICA_NR_LOKALU = U.DANE_TELEADRESOWE.DAN_ULICA_NR_LOKALU;                               
                    //dc.DANE_TELEADRESOWE.Add(D);
                    dc.KLIENCI.Add(k);  
                    dc.SaveChanges();
                    ModelState.Clear();
                    //D = null;
                    U = null;
                    k = null;
                    ViewBag.Message = "Successfully Registration Done";
                }
            }
            return View(U);
        }

        #endregion Rejestracja

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session["LogedUserID"] = null;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult BrakDostepu()
        {
            return View();
        }
    }
}
